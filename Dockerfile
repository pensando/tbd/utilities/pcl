FROM python:3.9-alpine

LABEL description="pcl"
LABEL version="0.0.1"
LABEL maintainer="techbd@pensando.io"

ADD $HOME/.psm/ /root/.psm/
ADD ./lib/openapi-generator-cli.jar /app/openapi-generator-cli.jar
ADD ./requirements.txt /app/requirements.txt
ADD ./pcl.py /app/pcl.py
ADD ./setup.py /app/setup.py
ADD ./lib /app/lib

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

WORKDIR /app

RUN apk update && apk upgrade && apk add bash && \
    ln -s /psm-tools/pyclient/apigroups apigroups && \
    ln -s /psm-tools/pyclient/utils utils && \
    pip install -U pip setuptools && \
    pip install --no-cache-dir --upgrade -r requirements.txt && \
    pip install --editable /app/.

RUN apk add build-base git make openjdk11 maven libffi-dev && \
    git clone https://github.com/pensando/psm-tools.git /psm-tools

WORKDIR /psm-tools/pyclient

RUN mkdir -p /psm-tools/pyclient/bin/ && \
    mv /app/openapi-generator-cli.jar /psm-tools/pyclient/bin/ && \
    #mkdir -p /psm-tools/pyclient/openapi-generator/modules/openapi-generator-cli/target/ && \
    # ln -s /psm-tools/pyclient/openapi-generator/bin/openapi-generator-cli.jar /psm-tools/pyclient/openapi-generator/modules/openapi-generator-cli/target/openapi-generator-cli.jar && \
    make
