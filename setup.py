from setuptools import setup


setup(
	name='pcl',
	version='1.0',
	py_modules=['pcl'],
	install_requires=[
		'Click',
	],
	entry_points='''
		[console_scripts]
		pcl=pcl:cli
	'''
)
