#!/usr/bin/env python
# Copyright (c) 2021, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ashwin Provine
#            Edward Arcuri edward@pensando.io
#

from lib.psm import telemetry
import os
import click
import warnings

from colorama.ansi import Fore
from lib.pclwrap import PSM
from apigroups.client.apis import ClusterV1Api,AuthV1Api,MonitoringV1Api
from apigroups.client import configuration, api_client

#
# Implement OpenAPI library for PSM and create login object
#
warnings.simplefilter("ignore")
HOME = os.environ['HOME']
configuration = configuration.Configuration(
    psm_config_path=HOME+"/.psm/config.json",
    interactive_mode=True)
configuration.verify_ssl = False
client = api_client.ApiClient(configuration)


@click.group()
def cli():
	pass

def print_help():
    ctx = click.get_current_context()
    click.echo("\n")
    click.echo(ctx.get_help())
    click.echo("\n")
    ctx.exit()

## AUTH
@cli.command()
@click.option('-f','--file',help="Filename to save output to (must use --json flag)")
@click.option('-j','--json',is_flag=True,help='Get the JSON output from the query')
@click.option('-l','--list',type=click.Choice(['users','roles'],case_sensitive=False),help='Get list of objects')
@click.option('-p','--policy',is_flag=True,help='View the authentication policy configured on PSM')
def auth(file,list,json,policy):
    """
    Authentication and Authorization information

    View policies, users and role bindings
    """
    api_instance = AuthV1Api(client)
    psm = PSM()

    if policy:
        try:
            response = psm.get_auth_policy(api_instance,json)
            if response:
                if file:
                    with open(file,"x") as f:
                        f.write(str(response))
                else:
                    print(response)
        except Exception as e:
            print(f"Error getting policy: {e}")
    if list:
        if 'users' in list:
            response = psm.get_users(api_instance,json)
            if response:
                if file:
                    with open(file,"x") as f:
                        f.write(str(response))
                else:
                    print(response)
        if 'roles' in list:
            response = psm.get_roles(api_instance,json)
            if response:
                if file:
                    with open(file,"x") as f:
                        f.write(str(response))
                else:
                    print(response)
    if not list and not policy:
        print_help()


##  CLUSTER
@cli.command()
@click.option('-f','--file',help="Filename to save output to (must use --json flag)")
@click.option('-i','--info',is_flag=True,help='Get cluster info (screen only)')
@click.option('-j','--json',is_flag=True,help='Get the JSON output from the query')
@click.option('-l','--license',is_flag=True,help='Get license of cluster')
@click.option('-p','--ping',is_flag=True,help='Get health status of PSM cluster')
@click.option('-v','--version',is_flag=True,help='Get version information of PSM cluster')
def cluster(file,info,json,license,ping,version):
    '''
    Configure and Manage the PSM cluster

    Manage nodes, DSCs, workloads, tenants, etc.
    '''
    api_instance = ClusterV1Api(client)
    psm = PSM()

    if info:
        try:
            response = psm.get_cluster_info(api_instance,json)
            if response:
                if file:
                    with open(file,"x") as f:
                        f.write(str(response))
                else:
                    print(response)
        except Exception as e:
                print(f"Unable to gather PSM cluster info: {e}")
    if ping:
        try:
            psm.ping_cluster(api_instance)
        except Exception as e:
            print(f"Error pinging cluster: {e}")
    if version:
        try:
            psm.get_cluster_version(api_instance)
        except Exception as e:
            print(f"Unable to obtain version from PSM cluster: {e}")
    if not info and not ping and not version:
        print_help()


## DSC
@cli.command()
@click.option('-f','--file',help="Filename to save output to (must use --json flag)")
@click.option('-i','--info',is_flag=True,help='Get health status of all DSCs')
@click.option('-j','--json',is_flag=True,help='Get the JSON output from the query')
def dsc(file,info,json):
    """
    Gather information about DSCs
    """
    api_instance = ClusterV1Api(client)
    psm = PSM()

    if info:
        response = psm.get_dsc_info(api_instance,json)
        if response:
            if file:
                with open(file,"x") as f:
                    f.write(str(response))
            else:
                print(response)
    if not info:
        print_help()

#ERSPAN
@cli.command()
@click.option('-a','--add',is_flag=True,help='TODO: Adds mirror session to configured ERSPAN sessions')
@click.option('-f','--file',help="Filename to save output to (must use --json flag)")
@click.option('-j','--json',is_flag=True,help='Get the JSON output from the query')
@click.option('-l','--list',is_flag=True,help='Gets the mirror session')
def erspan(add,file,list,json):
    """
    List/add/delete ERSPAN sessions
    """
    api_instance = MonitoringV1Api(client)
    psm = PSM()

    if add:
        response = psm.add_mirror(api_instance,json)
        if response:
            if file:
                with open(file,"x") as f:
                    f.write(str(response))
            else:
                print(response)

    if list:
        response = psm.list_mirror(api_instance,json)
        if response:
            if file:
                with open(file,"x") as f:
                    f.write(str(response))
            else:
                print(response)
    if not add and not list:
        print_help()


## LOGS - this corresponds to the objstore set of APIs in PSM
# @cli.command()
# @click.option('-f','--file',help="Filename to save CSV (must use --get flag)")
# @click.option('-e','--end_time',help="End of bounded time range to gather logs from",default='now')
# @click.option('-s','--start_time',help="Start of bounded time range to gather logs from",default='5 mins ago')
# def logs(file,end_time,start_time):
#     """Download CSV formatted firewall flowlogs from PSM

#        Download all logs within the bounded time range (default of last 5 minutes)
#     """
#     print_help()

## MONITOR
@cli.command()
@click.option('-e','--exportpolicies',is_flag=True,help='Get the export policies configured on PSM')
@click.option('-f','--file',help="Filename to save output to (must use --json flag)")
@click.option('-j','--json',is_flag=True,help='Get the JSON output from the query')
@click.option('-t','--tech',is_flag=True,help='Generate and retrive a tech support file')
@click.option('-v','--verbose',is_flag=True,help='verbose tag')
def monitor(file,tech,json,exportpolicies,verbose):
    """
    Configure and manage telemetry and logging

    Get Events, Stats, Logging, Alerts, Mirror Sessions, tech support files and other policy information
    """
    api_instance = MonitoringV1Api(client)
    psm = PSM()

    if tech:
        try:
            psm.get_tech_support_req(api_instance)
        except Exception as e:
            print(f"Error getting TechSupportRequest: {e}")
    elif exportpolicies:
        try:
            response = psm.get_export_policies(api_instance,json,verbose)
            if response:
                if file:
                    with open(file,"x") as f:
                        f.write(str(response))
                else:
                    print(response)
        except Exception as e:
            print(f"Error getting export policies: {e}")

    else:
        print_help()

## SECURITY
@cli.command()
@click.option('-f','--file',help="Filename to save retruned info to (must use --json flag)")
@click.option('-j','--json',help="Return raw response from API call")
def security(file,json):
    """
    Gather information on Network Security Policies

    """
    print_help()

## TELEMETRY
@cli.command()
@click.option('-f','--file',help="Filename to save retruned info to (must use --json flag)")
@click.option('-j','--json',help="Return raw response from API call")
def telemetry(file,json):
    """
    Get time bounded metrics about DSCs and traffic
    """
    print_help()


## WORKLOADS
@cli.command()
@click.option('-f','--file',help="Filename to save retruned info to (must use --json flag)")
@click.option('-j','--json',help="Return raw response from API call")
def workloads(file,json):
    """
    Get and Add information about Workloads

    Get workload names, labels, etc.
    Push labels to workloads
    """
    print_help()


cli.add_command(auth)
cli.add_command(cluster)
cli.add_command(dsc)
cli.add_command(erspan)
cli.add_command(monitor)
# cli.add_command(logs)
cli.add_command(security)
cli.add_command(telemetry)
cli.add_command(workloads)


if __name__ == "__main__":
	cli()
