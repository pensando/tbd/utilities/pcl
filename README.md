# Using the pcl container (recommended):

NOTE:  To use this option, you will need access to an Enterprise PSM.  It may work with Cloud, but has not yet been tested

- Make sure you have Docker installed on your host system

- In your HOME directory, create a directory named .psm and the config.json file therein (use editor of your choice)

    ```
    mkdir ~/.psm
    vi ~/.psm/config.json
    ```

    Place the following text in your config.json and edit the IP address field to your PSM IP

    ```json
    {
        "psm-ip": "your PSM IP"
    }
    ```

- Clone this repository and change into the directory created

- Build the container

    ```
    docker build . -t pcl
    ```

    This will use an alpine linux container, clone the [psm-tools repository](https://github.com/pensando/psm-tools/tree/main/pyclient), compile/make pyclient and install pcl for you

- Run the container

    ```
    docker run -it pcl /bin/sh
    ```

- The first time you run pcl it will ask for your login info for the PSM

</br>

# Running pcl locally:

- Install the OpenAPI pyclient per the instructions in Github

    [Pyclient on Github](https://github.com/pensando/psm-tools/tree/main/pyclient)

- make sure virtualenv venv is available to run and is installed

    ```python3 -m venv .venv```

- activate the virtual environment

    ```. .venv/bin/activate```

- link apigroups file from library

    ```ln -s ~/psm-tools/pyclient/apigroups```

- link utils file from library

    ```ln -s ~/psm-tools/pyclient/utils```

- upgrade pip

    ```pip install -U pip```

- install requirements

    ```pip install -r requirements.txt```

- install pcl using pip and setup.py file

    ```pip install --editable .```

- typing pcl in the cli should work now

- type deactivate to exit the venv and return to the regular terminal
