#!/usr/bin/env python
# Copyright (c) 2021, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ashwin Provine
#            Edward Arcuri edward@pensando.io
#
import json

from . import PSMBase
from colorama import Fore,Style


class MonitoringModule(PSMBase):
    """
    Provides stubs for calling APIs related to cluster endpoints in PSM
    """

    def get_tech_support_req(self,api_instance):
        """
        Get Cluster version information

        Args:
            api_instance (object): The API login object
        """
        response = api_instance.list_tech_support_request()
        print(response)
 #       print(f"\nPSM Cluster version is {response.status.build_version}")
 #       print(f"DSC auto rollout version is {response.spec.auto_rollout_dsc_version}\n")


    def get_export_policies(self,api_instance,flag,verbose):
        response = api_instance.list_flow_export_policy1()

        # If the user just wants the json, return that now and exit
        if flag:
            return response

        try:
            print(f"\n\nEXPORT POLICIES CURRENTLY CONFIGURED:")
            print("="*42)
            for policy in response.items:
                print(f"{Style.BRIGHT}{Fore.LIGHTWHITE_EX}{'Name:':15}\t\t{policy.meta.name} {Style.RESET_ALL}")
                export = f"{policy.spec.exports}"  if policy.spec.exports else 'N/A'
                export = export.replace('\n', '')
                export = export.replace('[{', '{')
                items = export.split('][')
                index = 0
                for i in items:
                    items[index] = items[index].replace('}]', '}~')
                    items[index] = items[index].replace('},', '}~')
                    jdata = items[index].split('~')
                    jindex = 0
                    for i in jdata:
                        jdata[jindex] = jdata[jindex].replace('\'','"')
                        if not jdata[jindex]:
                            continue
                        jstr = json.loads(jdata[jindex])
                        #print(json.dumps(jstr, sort_keys=False, indent=4))
                        for key in jstr:
                            print(f"{key:15}\t\t{jstr[key]}")
                        if verbose:
                                print(f"interval\t\t{policy.spec.interval}")
                                print(f"template interval\t{policy.spec.template_interval}")
                                print(f"match rules:")
                                #print(f"\t{policy.spec.match_rules}")
                                #with open("rules.json", "r") as read_file:
                                data = f"{policy.spec.match_rules}"#data = read_file.read()
                                data = data.replace("\'", "\"")
                                pdata = json.loads(data)
                                #pprint(jdata)
                                for i in range(len(pdata)):
                                    try:
                                        print("  " + str(pdata[i]["source"]["ip_addresses"][0]) + "\t" +
                                        str(pdata[i]["destination"]["ip_addresses"][0]) + "\t" +
                                        str(pdata[i]["app_protocol_selectors"]["proto_ports"]))
                                    except Exception as e:
                                        print(f"  any\tany\t['any']")

                        print("\n")
                        jindex = jindex+1
                    index = index+1
        except Exception as e:
            print(f"Unable to get Export Policies: {e}")
