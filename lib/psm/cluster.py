#!/usr/bin/env python
# Copyright (c) 2021, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ashwin Provine
#            Edward Arcuri edward@pensando.io
#

from . import PSMBase
from colorama import Fore,Style

class ClusterModule(PSMBase):
    """
    Provides stubs for calling APIs related to cluster endpoints in PSM
    """

    def get_cluster_info(self,api,flag):
        """
        getCluster Get information about PSM cluster

        :return: cluster info
        :rtype: json
        """
        response = api.get_cluster()
        number = api.list_node()
        version = api.get_version()

        # If the --json flag is sent, just return the output, else print it out
        # all pretty like to the screen
        if flag:
            return response
        else:
            print(f"{Style.BRIGHT}{Fore.LIGHTWHITE_EX}\n\nCLUSTER INFO{Style.RESET_ALL}")
            print(f"="*50)
            # Cluster Uptime
            uptime = (response.status.current_time - response.meta.creation_time)
            uptime_total_sec = uptime.total_seconds()
            uptime_days = int(uptime_total_sec//(24*60*60))
            uptime_hours = int(uptime_total_sec % (24*60*60))//(60*60)
            uptime_minutes = int(uptime_total_sec % (60*60))//60
            uptime_sec = int(uptime_total_sec % 60)
            nodes_unhealthy = False

            print(f"{Fore.LIGHTWHITE_EX}{'Uptime:':30} {Style.RESET_ALL}{uptime_days}d "
                    f"{uptime_hours}h {uptime_minutes}m {uptime_sec}s")
            print(f"{Fore.LIGHTWHITE_EX}{'Condition:':30} {Style.RESET_ALL}"
                    f"{response.status.conditions[0].type}")
            print(f"{Fore.LIGHTWHITE_EX}{'Number of nodes:':30} {Style.RESET_ALL}"
                    f"{number.list_meta.total_count}")
            print(f"{Fore.LIGHTWHITE_EX}{'PSM Cluster version:':30} {Style.RESET_ALL}"
                    f"{version.status.build_version}")
            print(f"{Fore.LIGHTWHITE_EX}{'DSC auto rollout version:':30}{Style.RESET_ALL} "
                    f"{version.spec.auto_rollout_dsc_version}\n")

            for node in response.status.quorum_status.members:
                print(f"{Fore.LIGHTWHITE_EX}{'Quorum Node:':30}{Style.RESET_ALL} {node.name}")
                print(f"{Fore.LIGHTWHITE_EX}{'Quorum Status:':30}{Style.RESET_ALL} {node.conditions[0].type}")
                node_health = node.conditions[0].type
                if node_health != "healthy":
                    nodes_unhealthy = True

            if not nodes_unhealthy:
                print(f"\n{Fore.LIGHTWHITE_EX}All nodes are healthy!!!\n{Style.RESET_ALL}")
            else:
                print(f"\n{Fore.LIGHTWHITE_EX}Not all nodes are healthy!!!\n{Style.RESET_ALL}")


    def ping_cluster(self,api):
        """
        Ping the cluster and make sure it is up and nodes have a status of
        healthy

        Args:
            api (object): API instance of ClusterV1API
        """

        response = api.get_cluster()

        # Cluster Uptime
        uptime = (response.status.current_time - response.meta.creation_time)
        uptime_total_sec = uptime.total_seconds()
        uptime_days = int(uptime_total_sec//(24*60*60))
        uptime_hours = int(uptime_total_sec % (24*60*60))//(60*60)
        uptime_minutes = int(uptime_total_sec % (60*60))//60
        uptime_sec = int(uptime_total_sec % 60)

        print("\n")
        print(f"{Fore.LIGHTWHITE_EX}{'Uptime:':30} {Style.RESET_ALL}{uptime_days}d "
                f"{uptime_hours}h {uptime_minutes}m {uptime_sec}s")
        print(f"{Fore.LIGHTWHITE_EX}{'Condition:':30} {Style.RESET_ALL}"
                f"{response.status.conditions[0].type}")

        nodes_unhealthy = False
        for node in response.status.quorum_status.members:
                node_health = node.conditions[0].type
                if node_health != "healthy":
                        nodes_unhealthy = True
                        print(f"\tCluster Quorum Node: ", node.name)
                        print(f"\tCluster Quorum Status: ", node.conditions[0].type)
        if not nodes_unhealthy:
                print(f"\nAll nodes are healthy\n")


    def get_cluster_version(self,api_instance):
        """
        Get Cluster version information

        Args:
            api_instance (object): The API login object
        """
        response = api_instance.get_version()
        print(response)
        print(f"\n\n{Fore.LIGHTWHITE_EX}{'PSM Cluster version:':30} {Style.RESET_ALL}"
                    f"{response.status.build_version}")
        print(f"{Fore.LIGHTWHITE_EX}{'DSC auto rollout version:':30}{Style.RESET_ALL} "
                f"{response.spec.auto_rollout_dsc_version}\n")
