#!/usr/bin/env python
# Copyright (c) 2021, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ashwin Provine
#            Edward Arcuri edward@pensando.io
#


class PSMBase:
    """A base class for API categories: Classes that provide methods which call the
    API to provide some functionality. Included are methods to make
    communicating with the API easier and also utility methods to do things with
    the output from the API.
    """

    def sendLog(host, port, msg):
        """
        Send a syslog msg using UDP to the specified host on the specified port

        :param host: host ip/name to send the syslog message to
        :type host: string

        :param port: port number to send UDP syslog msg to
        :type port: integer

        :param msg: the message to send in the syslog
        :type msg: string

        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)   # UDP
        sock.sendto(bytes(msg, "utf-8"), (host, port))
        sock.close()


    def randomLine(file):
        """
        Pull a random line from the specified file

        :param file: fully qualified path to file to pull the line from
        :type file: string

        """
        line = next(file)
        for num, aline in enumerate(file):
            if random.randrange(num + 2): continue
            line = aline
        return line


    def calcDate(direction,ndays=0,dateFormat="n"):
        """
        Calculate the date as of now, days in the past or days in the future

        Args:
            direction (string): Valid options are
                                "past" - calculate timestamp to n days in the past
                                "now" - calculate the current timestamp
                                "future" - calculate the timestamp n days in the future
            ndays (int, optional): Number of days past or future to calculate the timestamp. Defaults to 0.
            dateFormat (str, optional): If "T" is sent it will return the "%Y/%m/%d %H:%M:%SZ" format.
                                        If nothing is sent (default) it will return the "%Y/%m/%d %H:%M:%SZ" format.

        Returns:
            string: the calculated date timestamp in the format specified
        """
        if dateFormat == "T":
            if direction == "past":
                return f"{datetime.now(tz=timezone.utc) - timedelta(days=ndays):%Y/%m/%d %H:%M:%SZ}"
            elif direction == "now":
                return f"{datetime.now(tz=timezone.utc):%Y-%m-%dT%H:%M:%SZ}"
            else:
                return f"{datetime.now(tz=timezone.utc) + timedelta(days=ndays):%Y-%m-%dT%H:%M:%SZ}"
        else:
            if direction == "past":
                return f"{datetime.now(tz=timezone.utc) - timedelta(days=ndays):%Y/%m/%d %H:%M:%SZ}"
            elif direction == "now":
                return f"{datetime.now(tz=timezone.utc):%Y/%m/%d T%H:%M:%SZ}"
            else:
                return f"{datetime.now(tz=timezone.utc) + timedelta(days=ndays):%Y/%m/%d %H:%M:%SZ}"


    def strTimeProp(start, end, dateFormat, prop):
        """
        Get a time at a proportion of a range of two formatted times.

        Args:
            start (string): strftime-style timestamp of the start time
            end (string): strftime-style timestamp of the end time
            dateFormat (string): specifies the returned timestamp format
            prop (string): specifies how a proportion of the interval to be taken
                           after start.
        """

        stime = time.mktime(time.strptime(start, dateFormat))
        etime = time.mktime(time.strptime(end, dateFormat))
        ptime = stime + prop * (etime - stime)
        return time.strftime(dateFormat, time.localtime(ptime))


    def randomDate(start, end, prop,dateFormat='%Y/%m/%d %H:%M:%S'):
        """
        Gnerate a random date string in the given format

        Args:
            start (string): Timestamp start boundary time
            end (string): Timestamp stop boundary time
            prop (string): the proportion after the start time to get timestamp
            dateFormat (str, optional): Timestamp format. Defaults to '%Y/%m/%d %H:%M:%S'.

        Returns:
            string: the timestamp in the requested format
        """
        return strTimeProp(start, end, dateFormat, prop).strip()


    def returnTime(now, t5):
        # return_time formats string for use with PSM API start and endtime

        fmonth = now.strftime("%m")
        day = now.strftime("%d")
        hour = now.strftime("%H")
        minute = now.strftime("%M")
        second = now.strftime("%S")
        tDash = f"{now.year}-{fmonth}-{day}T{hour}:{minute}:{second}Z"
        fmonth = t5.strftime("%m")
        hour = t5.strftime("%H")
        minute = t5.strftime("%M")
        second = t5.strftime("%S")

        tminusDash = f"{now.year}-{fmonth}-{day}T{hour}:{minute}:{second}Z"

        return tDash, tminusDash
