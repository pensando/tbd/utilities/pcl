#!/usr/bin/env python
# Copyright (c) 2021, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ashwin Provine
#            Edward Arcuri edward@pensando.io
#

import pytz
import datetime

from . import PSMBase
from colorama import Fore,Style


class MirroringModule(PSMBase):

    def list_mirror(self,api_instance,flag):
        response = api_instance.list_mirror_session1()
        if flag:
            return response
        else:
            tz = pytz.timezone('UTC')
            print(f"\n\n{Style.BRIGHT}{Fore.LIGHTWHITE_EX}CONFIGURED ERSPAN SESSIONS{Style.RESET_ALL}")
            print("="*90)
            print(f"{Fore.LIGHTWHITE_EX}{'Name':22}{'ID':8}{'Destination':20}{'Type':20}{'Uptime':20}{Style.RESET_ALL}")
            print("="*90)

            for session in response.items:
                uptime = (datetime.datetime.now(tz=tz) - session.status.started_at)
                uptime_total_sec = uptime.total_seconds()
                uptime_days = int(uptime_total_sec//(24*60*60))
                uptime_hours = int(uptime_total_sec % (24*60*60))//(60*60)
                uptime_minutes = int(uptime_total_sec % (60*60))//60
                uptime_sec = int(uptime_total_sec % 60)
                if str(session.spec.disabled) == 'True':
                    print(f"{session.meta.name:22}{session.spec.span_id:<8}{session.spec.collectors[0]['export_config']['destination']:20}"
                          f"{session.spec.collectors[0]['type']:20}{'Session Disabled':<20}")
                else:
                    print(f"{session.meta.name:22}{session.spec.span_id:<8}{session.spec.collectors[0]['export_config']['destination']:20}"
                          f"{session.spec.collectors[0]['type']:20}{uptime_days}d {uptime_hours}h {uptime_minutes}m {uptime_sec}s")
            print(f"\n\n")


    def add_mirror(self,api_instance,flag):
        response = api_instance.add_mirror_session1()
        if flag:
            return response
        else:
            print(response)
