#!/usr/bin/env python
# Copyright (c) 2021, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ashwin Provine
#            Edward Arcuri edward@pensando.io
#

from . import PSMBase
from colorama import Fore,Style


class AuthModule(PSMBase):
    """
    Provides stubs for calling APIs related to auth endpoints in PSM
    """
    def get_auth_policy(self,api_instance,flag):
        """
        Get the authentication policy for the PSM cluster

        :return: Authentication Policy
        :rtype: json
        """
        response = api_instance.get_authentication_policy()

        if flag:
            return response
        else:
            try:
                print(f"\n\n{Style.BRIGHT}{Fore.LIGHTWHITE_EX}AUTHENTICATION POLICY{Style.RESET_ALL}\n")
                print(f"="*40)
                print(f"{Fore.LIGHTWHITE_EX}Authenticators{Style.RESET_ALL}")
                print(f"="*40)
                print(f"{Fore.LIGHTWHITE_EX}{'  Authentication Order:':25}{Style.RESET_ALL}{response.spec.authenticators.authenticator_order}")
                print(f"{Fore.LIGHTWHITE_EX}{'  LDAP:':25}{Style.RESET_ALL}{response.spec.authenticators.ldap}")
                print(f"{Fore.LIGHTWHITE_EX}{'  Local:':25}{Style.RESET_ALL}{response.spec.authenticators.local}")
                print(f"{Fore.LIGHTWHITE_EX}{'  Token Expiry:':25}{Style.RESET_ALL}{response.spec.token_expiry}\n\n")
            except Exception as e:
                print(f"Unable to get Authentication Policy: {e}")

    def get_role_bindings(self):
        """
        Get the role bindings for the users

        :return: Role bindings for all users
        :rtype: json
        """
        pass


    def get_roles(self,api_instance,flag):
        """
        Get info dump of all role bindings

        :return: Information of all role bindings
        :rtype: json
        """
        response = api_instance.list_role_binding1()

        if flag:
            return response
        else:
            print(f"\n\n{Style.BRIGHT}{Fore.LIGHTWHITE_EX}ROLE BINDINGS{Style.RESET_ALL}\n")
            print(f"="*70)
            print(f"{Fore.LIGHTWHITE_EX}{'Role:':20}Users:{Style.RESET_ALL}")
            print(f"="*70)
            try:
                for user in response.items:
                    role = f"{user.spec.role}" if user.spec.role else '[ N/A ]'
                    try:
                        users = f"{user.spec.users}" if user.spec.users else '[ N/A ]'
                    except Exception:
                        users = '[ N/A ]'
                    print(f"{role:20}{users}")
                print(f"\n")
            except Exception as e:
                print(f"Unable to get role or users: {e}")


    def get_user(self,user):
        """
        Get info dump for particular user

        :return: Information on user specified
        :rtype: json
        """
        pass


    def get_users(self,api_instance,flag):
        """
        Get info dump of all users

        :return: Information on all users in tenant
        :rtype: json
        """
        response = api_instance.list_user1()

        if flag:
            return response
        else:
            print(f"\n\n{Style.BRIGHT}{Fore.LIGHTWHITE_EX}USERS{Style.RESET_ALL}\n")
            print(f"="*65)
            name = "Name:"
            email = "Email:"
            login = "Login:"
            print(f"{Fore.LIGHTWHITE_EX}{name:<20}\t{email:<20}\t{login:<20}{Style.RESET_ALL}")
            print(f"="*65)
            try:
                for user in response.items:
                    try:
                        name = f"{user.spec.fullname}" if user.spec.fullname else 'N/A'
                    except Exception:
                        name = 'N/A'
                    try:
                        email = f"{user.spec.email}" if user.spec.email else 'N/A'
                    except Exception:
                        email = 'N/A'
                    login = f"{user.meta.name}"
                    print(f"{name:<20}\t{email:<20}\t{login}")
                print("\n")
            except Exception as e:
                print(f"Unable to get name, email, or login: {e}")


    def getUserPrefs(self,user):
        """
        Get the user preferences for the specified user

        :param user: username of user to get preferences for
        :type user: string
        :return: user preferences
        :rtype: json
        """
        pass
