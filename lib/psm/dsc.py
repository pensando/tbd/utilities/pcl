#!/usr/bin/env python
# Copyright (c) 2021, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ashwin Provine
#            Edward Arcuri edward@pensando.io
#

from . import PSMBase
from colorama import Fore,Style


class DSCModule(PSMBase):


    def get_dsc_info(self,apiClient,flag):
        response = apiClient.list_distributed_service_card()

        if flag:
            return response
        else:
            print(f"\n\n{Style.BRIGHT}{Fore.LIGHTWHITE_EX}DSC INVENTORY{Style.RESET_ALL}")
            print(f"="*70)
            print(f"{Style.BRIGHT}{Fore.LIGHTWHITE_EX}{'Name':20}{'MAC':20}{'Version':15}{'Status':10}{Style.RESET_ALL}")
            print(f"="*70)
            for dsc in response.items:
                if "conditions" in dsc.status and len(dsc.status.conditions)>0:
                    dscStatus = dsc.status.conditions[0].type
                else:
                    dscStatus = "unknown"

                try:
                    dscID = dsc.spec.id
                except:
                    dscID = "N/A"
                try:
                    dscName = dsc.meta.name
                except:
                    dscName = "N/A"
                try:
                    dscVer = dsc.status.dsc_version
                except:
                    dscVer = "N/A"

                print(f"{dscID:20s}{dscName:20s}{dscVer:15s}{dscStatus:10s}")
            print(f"\n")
